package org.tvolkov.revolut.database;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.tvolkov.revolut.properties.PropertiesHolder;

import javax.sql.DataSource;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class BasicDataSourceProviderTest {

    @Mock
    private PropertiesHolder propertiesHolder;

    @InjectMocks
    private BasicDataSourceProvider basicDataSourceProvider;

    @Test
    public void shouldReturnDataSource(){
        //when
        DataSource dataSource = basicDataSourceProvider.getDataSource();

        //then
        assertNotNull(dataSource);
    }
}