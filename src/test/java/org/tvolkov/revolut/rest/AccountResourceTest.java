package org.tvolkov.revolut.rest;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.tvolkov.revolut.model.Account;
import org.tvolkov.revolut.rest.dto.AccountDto;
import org.tvolkov.revolut.rest.exceptions.AccountLookupException;
import org.tvolkov.revolut.rest.exceptions.NoAccountFoundException;
import org.tvolkov.revolut.rest.providers.AccountDtoValidationExceptionMapper;
import org.tvolkov.revolut.rest.providers.AccountLookupExceptionMapper;
import org.tvolkov.revolut.service.AccountDtoValidationException;
import org.tvolkov.revolut.service.AccountService;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.*;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AccountResourceTest extends JerseyTest {

    private static final String ACCOUNT = "/account";

    @Mock
    private AccountService accountService;

    @Mock
    private UriInfo uriInfo;

    @Override
    public Application configure() {
        MockitoAnnotations.initMocks(this);
        enable(TestProperties.LOG_TRAFFIC);
        enable(TestProperties.DUMP_ENTITY);
        return new ResourceConfig()
                .register(AccountResource.class)
                .register(AccountLookupExceptionMapper.class)
                .register(AccountDtoValidationExceptionMapper.class)
                .register(new AbstractBinder() {
                    @Override
                    protected void configure() {
                        bind(accountService).to(AccountService.class);
                    }
                });
    }

    @Test
    public void shouldReturnAccountAnd200Response() {
        //given
        Account account = new Account(1L, new BigDecimal("100.50"), "33344");
        when(accountService.getAccount("1")).thenReturn(account);

        //when
        Response response = target(ACCOUNT + "/1").request().get();

        //then
        assertEquals("should return 200", 200, response.getStatus());
        assertEquals(account, response.readEntity(Account.class));
    }

    @Test
    public void shouldReturn404IfAccountNotFound() {
        //given
        String errorMsg = "no account with given id was found";
        when(accountService.getAccount("1")).thenThrow(new AccountLookupException(new NoAccountFoundException(errorMsg), Response.Status.NOT_FOUND));

        //when
        Response response = target(ACCOUNT + "/1").request().get();

        //when
        assertEquals("should return 404", 404, response.getStatus());
        assertEquals(errorMsg, response.readEntity(String.class));
    }

    @Test
    public void shouldReturn500IfInternalServerErrorHasOccurred() {
        //given
        String errorMsg = "error reading account from database";
        when(accountService.getAccount("1")).thenThrow(new AccountLookupException(new SQLException(errorMsg), Response.Status.INTERNAL_SERVER_ERROR));

        //when
        Response response = target(ACCOUNT + "/1").request().get();

        //then
        assertEquals("should return 500", 500, response.getStatus());
        assertNotNull(response.getEntity());
    }

    @Test
    public void shouldListAccountsReturn500IfErrorOccursWhenGettingAccountsList() {
        //given
        String errorMsg = "error reading accounts list from database";
        when(accountService.listAccounts()).thenThrow(new AccountLookupException(new SQLException(errorMsg), Response.Status.INTERNAL_SERVER_ERROR));

        //when
        Response response = target(ACCOUNT).request().get();

        //then
        assertEquals("should return 500", 500, response.getStatus());
        assertNotNull(response.getEntity());
    }

    @Test
    public void shouldListAccountsReturn200AndListOfAccounts() {
        //given
        List<Account> accounts = new ArrayList<Account>() {{
            add(new Account(1L, new BigDecimal("100.00"), "1234"));
        }};
        when(accountService.listAccounts()).thenReturn(accounts);

        //when
        Response response = target(ACCOUNT).request().get();

        //then
        assertEquals("should return 200", 200, response.getStatus());
        assertEquals(accounts.size(), response.readEntity(List.class).size());
    }

    @Test
    public void shouldCreateAccountReturn200AndIdOfCreatedAccount() {
        //given
        AccountDto accountDto = new AccountDto("100.00", "2233");
        when(accountService.createAccount(any(AccountDto.class))).thenReturn(1L);
        UriBuilder uriBuilder = mock(UriBuilder.class);
        when(uriInfo.getAbsolutePathBuilder()).thenReturn(uriBuilder);

        //when
        Response response = target(ACCOUNT).request().post(Entity.entity(accountDto, MediaType.APPLICATION_JSON));

        //then
        assertEquals("should return 201", 201, response.getStatus());
    }

    @Test
    public void shouldCreateAccountReturn400IfAccountDtoIsInvalid() {
        //given
        AccountDto accountDto = new AccountDto("-100.00", "2233");
        when(accountService.createAccount(any(AccountDto.class))).thenThrow(new AccountDtoValidationException("Invalid dto"));

        //when
        Response response = target(ACCOUNT).request().post(Entity.entity(accountDto, MediaType.APPLICATION_JSON));

        //then
        assertEquals("should return 400", 400, response.getStatus());
    }

    @Test
    public void shouldCreateAccountReturn500IfErrorOccurs() {
        //given
        AccountDto accountDto = new AccountDto("100.00", "1234");
        when(accountService.createAccount(any(AccountDto.class))).thenThrow(new AccountLookupException(new SQLException("DB error occurred"), Response.Status.INTERNAL_SERVER_ERROR));

        //when
        Response response = target(ACCOUNT).request().post(Entity.entity(accountDto, MediaType.APPLICATION_JSON));

        //then
        assertEquals("should return 500", 500, response.getStatus());
    }

    @Test
    public void shouldDeleteAccountReturn500IfErrorOccurs(){
        //when
        Response response = target(ACCOUNT + "/1").request().delete();

        //then
        assertEquals("should return 204", 204, response.getStatus());
    }

    @Test
    public void shouldReturnAccountReturn204IfDeletedSuccessfully(){
        //given
        doThrow(new AccountLookupException(new SQLException("DB Error occurred"), Response.Status.INTERNAL_SERVER_ERROR)).when(accountService).deleteAccount("1");

        //when
        Response response = target(ACCOUNT + "/1").request().delete();

        //then
        assertEquals("should return 500", 500, response.getStatus());
    }
}