package org.tvolkov.revolut.rest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.tvolkov.revolut.rest.dto.TransferData;
import org.tvolkov.revolut.rest.exceptions.TransferDataValidationException;
import org.tvolkov.revolut.service.TransferDataValidator;

import java.math.BigDecimal;

@RunWith(JUnit4.class)
public class TransferDataValidatorTest {

    private TransferDataValidator transferDataValidator;

    @Before
    public void init(){
        transferDataValidator = new TransferDataValidator();
    }

    @Test(expected = TransferDataValidationException.class)
    public void shouldThrowExceptionWhenAmountIsNull() throws TransferDataValidationException {
        //given
        TransferData transferData = new TransferData(null, null, null);

        //when
        transferDataValidator.validateTransferData(transferData);
    }

    @Test(expected = TransferDataValidationException.class)
    public void shouldThrowExceptionWhenAmountIsNegative() throws TransferDataValidationException {
        //given
        BigDecimal amount = new BigDecimal("-12345");
        TransferData transferData = new TransferData(amount, null, null);

        //when
        transferDataValidator.validateTransferData(transferData);
    }

    @Test(expected = TransferDataValidationException.class)
    public void shouldThrowExceptionWhenSourceAccountIsNull() throws TransferDataValidationException {
        //given
        BigDecimal amount = new BigDecimal("12345");
        TransferData transferData = new TransferData(amount, null, null);

        //when
        transferDataValidator.validateTransferData(transferData);
    }

    @Test(expected = TransferDataValidationException.class)
    public void shouldThrowExceptionWhenSourceAccountIsEmpty() throws TransferDataValidationException {
        //given
        BigDecimal amount = new BigDecimal("12345");
        String srcAccount = "";
        TransferData transferData = new TransferData(amount, srcAccount, null);

        //when
        transferDataValidator.validateTransferData(transferData);
    }

    @Test(expected = TransferDataValidationException.class)
    public void shouldThrowExceptionWhenDestinationAccountIsNull() throws TransferDataValidationException {
        //given
        BigDecimal amount = new BigDecimal("12345");
        String srcAccount = "111222";
        TransferData transferData = new TransferData(amount, srcAccount, null);

        //when
        transferDataValidator.validateTransferData(transferData);
    }

    @Test(expected = TransferDataValidationException.class)
    public void shouldThrowExceptionWhenDestinationAccountIsEmpty() throws TransferDataValidationException {
        //given
        BigDecimal amount = new BigDecimal("12345");
        String srcAccount = "111222";
        String dstAccount = "";
        TransferData transferData = new TransferData(amount, srcAccount, dstAccount);

        //when
        transferDataValidator.validateTransferData(transferData);
    }

    @Test
    public void shouldNotThrowExceptionsIfTransferDataIsValid() throws TransferDataValidationException {
        //given
        BigDecimal amount = new BigDecimal("12345");
        String srcAccount = "111222";
        String dstAccount = "333444";
        TransferData transferData = new TransferData(amount, srcAccount, dstAccount);

        //when
        transferDataValidator.validateTransferData(transferData);
    }
}