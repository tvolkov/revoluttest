package org.tvolkov.revolut.rest;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.tvolkov.revolut.rest.dto.TransferData;
import org.tvolkov.revolut.rest.dto.TransferResult;
import org.tvolkov.revolut.rest.providers.AccountLookupExceptionMapper;
import org.tvolkov.revolut.rest.providers.TransferExceptionMapper;
import org.tvolkov.revolut.service.AccountService;
import org.tvolkov.revolut.service.TransferException;
import org.tvolkov.revolut.service.TransferService;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.math.BigDecimal;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Mockito.when;

public class TransferResourceTest extends JerseyTest {

    @Mock
    private TransferService transferService;


    @Override
    public Application configure() {
        MockitoAnnotations.initMocks(this);
        enable(TestProperties.LOG_TRAFFIC);
        enable(TestProperties.DUMP_ENTITY);
        return new ResourceConfig()
                .register(TransferResource.class)
                .register(TransferExceptionMapper.class)
                .register(new AbstractBinder() {
                    @Override
                    protected void configure() {
                        bind(transferService).to(TransferService.class);
                    }
                });
    }

    @Test
    public void shouldReturn200WhenTransferHasBeenPerformed(){
        //given
        TransferData transferData = new TransferData(new BigDecimal("100"), "123", "456");
        TransferResult transferResult = new TransferResult(true, null, "1000", "123", "456");
        when(transferService.transfer(any(TransferData.class))).thenReturn(transferResult);

        //when
        Response response = target("/transfer").request().post(Entity.entity(transferData, MediaType.APPLICATION_JSON));

        //then
        assertEquals("should return 200", 200, response.getStatus());
        assertEquals(transferResult, response.readEntity(TransferResult.class));
    }

    @Test
    public void shouldReturn400WhenInputDataIsInvalid(){
        //given
        TransferData transferData = new TransferData(new BigDecimal("100"), "123", "456");
        TransferResult transferResult = new TransferResult(false, "Input data is invalid", "100", "123","456");
        when(transferService.transfer(any(TransferData.class))).thenThrow(new TransferException(Response.Status.BAD_REQUEST, transferResult));

        //when
        Response response = target("/transfer").request().post(Entity.entity(transferData, MediaType.APPLICATION_JSON));

        //then
        assertEquals("should return 400", 400, response.getStatus());
        assertEquals(transferResult, response.readEntity(TransferResult.class));
    }

    @Test
    public void shouldReturn406WhenInsufficientFundsOnSourceAccount(){
        //given
        TransferData transferData = new TransferData(new BigDecimal("100"), "123", "456");
        TransferResult transferResult = new TransferResult(false, "Insufficient funds", "100", "123","456");
        when(transferService.transfer(any(TransferData.class))).thenThrow(new TransferException(Response.Status.NOT_ACCEPTABLE, transferResult));

        //when
        Response response = target("/transfer").request().post(Entity.entity(transferData, MediaType.APPLICATION_JSON));

        //then
        assertEquals("should return 406", 406, response.getStatus());
        assertEquals(transferResult, response.readEntity(TransferResult.class));
    }

    @Test
    public void shouldReturn500WhenDBErrorOccurs(){
        //given
        TransferData transferData = new TransferData(new BigDecimal("100"), "123", "456");
        TransferResult transferResult = new TransferResult(false, "Error while executing transfer", "100", "123","456");
        when(transferService.transfer(any(TransferData.class))).thenThrow(new TransferException(Response.Status.INTERNAL_SERVER_ERROR, transferResult));

        //when
        Response response = target("/transfer").request().post(Entity.entity(transferData, MediaType.APPLICATION_JSON));

        //then
        assertEquals("should return 500", 500, response.getStatus());
        assertEquals(transferResult, response.readEntity(TransferResult.class));

    }
}