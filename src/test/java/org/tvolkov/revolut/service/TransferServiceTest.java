package org.tvolkov.revolut.service;

import org.h2.value.Transfer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.tvolkov.revolut.dao.AccountNotFoundException;
import org.tvolkov.revolut.dao.InsufficientFundsException;
import org.tvolkov.revolut.dao.TransferOperation;
import org.tvolkov.revolut.rest.dto.TransferData;
import org.tvolkov.revolut.rest.dto.TransferResult;
import org.tvolkov.revolut.rest.exceptions.TransferDataValidationException;

import java.math.BigDecimal;
import java.sql.SQLException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TransferServiceTest {

    @Mock
    private TransferDataValidator transferDataValidator;

    @Mock
    private TransferOperation transferOperation;

    @InjectMocks
    private TransferService transferService;

    @Test(expected = TransferException.class)
    public void shouldThrowExceptionIfTransferDataIsInvalid() throws TransferDataValidationException, TransferException {
        //given
        TransferData transferData = createTransferData();
        doThrow(TransferDataValidationException.class).when(transferDataValidator).validateTransferData(transferData);

        //when
        transferService.transfer(transferData);
    }


    @Test(expected = TransferException.class)
    public void shouldThrowExceptionIfErrorOccursDuringTransaction() throws SQLException, TransferException, InsufficientFundsException, AccountNotFoundException {
        //given
        TransferData transferData = createTransferData();
        doThrow(SQLException.class).when(transferOperation).performTransfer(any(), any(), any());

        //when
        transferService.transfer(transferData);
    }

    @Test(expected = TransferException.class)
    public void shouldThrowExceptionIfThereWereInsufficientFundsOnSrcAccount() throws InsufficientFundsException, AccountNotFoundException, SQLException {
        //given
        TransferData transferData = createTransferData();
        doThrow(InsufficientFundsException.class).when(transferOperation).performTransfer(any(), any(), any());

        //when
        transferService.transfer(transferData);
    }

    @Test(expected = TransferException.class)
    public void shouldThrowExceptionIfAccountWasNotFound() throws InsufficientFundsException, AccountNotFoundException, SQLException {
        //given
        TransferData transferData = createTransferData();
        doThrow(AccountNotFoundException.class).when(transferOperation).performTransfer(any(), any(), any());

        //when
        transferService.transfer(transferData);

    }

    @Test
    public void shouldReturnSuccessfulResultIfTransactionSucceeds() throws SQLException, TransferException, TransferDataValidationException, InsufficientFundsException, AccountNotFoundException {
        //given
        TransferData transferData = createTransferData();
        doNothing().when(transferDataValidator).validateTransferData(transferData);
        doNothing().when(transferOperation).performTransfer(any(), any(), any());

        //when
        TransferResult transferResult = transferService.transfer(transferData);

        //then
        assertNotNull(transferResult);
        assertTrue(transferResult.isSuccess());
        assertEquals(transferData.getAmount().toString(), transferResult.getAmount());
        assertEquals(transferData.getFrom(), transferResult.getFromAcc());
        assertEquals(transferData.getTo(), transferResult.getToAcc());
    }

    private TransferData createTransferData(){
        return new TransferData(new BigDecimal("100"), "1", "2");
    }
}