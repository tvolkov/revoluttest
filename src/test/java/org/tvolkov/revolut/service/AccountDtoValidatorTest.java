package org.tvolkov.revolut.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.tvolkov.revolut.rest.dto.AccountDto;

@RunWith(JUnit4.class)
public class AccountDtoValidatorTest {

    private AccountDtoValidator accountDtoValidator;

    @Before
    public void init() {
        accountDtoValidator = new AccountDtoValidator();
    }

    @Test(expected = AccountDtoValidationException.class)
    public void shouldThrowExceptionIfAccountDtoIsNull() {
        //when
        accountDtoValidator.validateAccountDto(null);
    }

    @Test(expected = AccountDtoValidationException.class)
    public void shouldThrowExceptionIfAmountIsNull() {
        //given
        AccountDto accountDto = new AccountDto();

        //when
        accountDtoValidator.validateAccountDto(accountDto);
    }

    @Test(expected = AccountDtoValidationException.class)
    public void shouldThrowExceptionIfAmountIsEmpty() {
        //given
        AccountDto accountDto = new AccountDto("", null);

        //when
        accountDtoValidator.validateAccountDto(accountDto);
    }

    @Test(expected = AccountDtoValidationException.class)
    public void shouldThrowExceptionIfAmountIsNegative() {
        //given
        AccountDto accountDto = new AccountDto("-1", null);

        //when
        accountDtoValidator.validateAccountDto(accountDto);
    }

    @Test(expected = AccountDtoValidationException.class)
    public void shouldThrowExceptionIfAccountNumberIsNull() {
        //given
        AccountDto accountDto = new AccountDto("100.00", null);

        //when
        accountDtoValidator.validateAccountDto(accountDto);
    }

    @Test(expected = AccountDtoValidationException.class)
    public void shouldThrowExceptionIfAccountNumberIsBlank() {
        //given
        AccountDto accountDto = new AccountDto("100.00", "");

        //when
        accountDtoValidator.validateAccountDto(accountDto);
    }

    @Test
    public void shouldNotThrowExceptionIfAccountDtoIsValid() {
        //given
        AccountDto accountDto = new AccountDto("100.00", "5678");

        //when
        accountDtoValidator.validateAccountDto(accountDto);
    }
}