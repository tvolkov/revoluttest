package org.tvolkov.revolut.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.tvolkov.revolut.dao.AccountDao;
import org.tvolkov.revolut.model.Account;
import org.tvolkov.revolut.rest.dto.AccountDto;
import org.tvolkov.revolut.rest.exceptions.AccountLookupException;
import org.tvolkov.revolut.rest.exceptions.NoAccountFoundException;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {

    @Mock
    private AccountDtoValidator accountDtoValidator;

    @Mock
    private AccountDao accountDao;

    @InjectMocks
    private AccountService accountService;

    @Test
    public void shouldReturnAccountWhenGetAccount() throws SQLException, NoAccountFoundException {
        //given
        Account expectedAccount = new Account(1L, new BigDecimal("100.00"), "1234");
        when(accountDao.findByAccountId(String.valueOf(expectedAccount.getId()))).thenReturn(expectedAccount);

        //when
        Account actualAccount = accountService.getAccount(String.valueOf(expectedAccount.getId()));

        //then
        assertEquals(expectedAccount, actualAccount);
    }

    @Test(expected = AccountLookupException.class)
    public void shouldThrowExceptionIfDBErrorOccurs() throws SQLException, NoAccountFoundException {
        //given
        when(accountDao.findByAccountId(anyString())).thenThrow(SQLException.class);

        //when
        accountService.getAccount("1");
    }

    @Test(expected = AccountLookupException.class)
    public void shouldThrowExceptionIfNoAccountFound() throws SQLException, NoAccountFoundException {
        //given
        when(accountDao.findByAccountId(anyString())).thenThrow(NoAccountFoundException.class);

        //when
        accountService.getAccount("1");
    }

    @Test(expected = AccountDtoValidationException.class)
    public void shouldThrowExceptionIfAccountDtoIsInvalidWhenCreateAccount(){
        //given
        doThrow(AccountDtoValidationException.class).when(accountDtoValidator).validateAccountDto(any(AccountDto.class));

        //when
        accountService.createAccount(new AccountDto());
    }

    @Test
    public void shouldReturnNewAccountIdWhenCreateAccount() throws SQLException {
        //given
        AccountDto accountDto = new AccountDto();
        long newAccId = 1L;
        when(accountDao.createAccount(accountDto)).thenReturn(newAccId);

        //when
        long actualId = accountService.createAccount(accountDto);

        //then
        assertEquals(newAccId, actualId);
    }

    @Test(expected = AccountLookupException.class)
    public void shouldThrowExceptionIfDBErrorOccursWhenCreateAccount() throws SQLException {
        //given
        AccountDto accountDto = new AccountDto();
        when(accountDao.createAccount(accountDto)).thenThrow(SQLException.class);

        //when
        accountService.createAccount(accountDto);
    }

    @Test
    public void shouldReturnListOfAccounts() throws SQLException {
        //given
        List<Account> expectedAcounts = new ArrayList<Account>(){{
            add(new Account(1L, new BigDecimal("100.00"), "1234"));
        }};
        when(accountDao.listAccounts()).thenReturn(expectedAcounts);

        //when
        List<Account> actualAccounts = accountService.listAccounts();

        //then
        assertEquals(expectedAcounts.size(), actualAccounts.size());
        assertEquals(expectedAcounts.get(0), actualAccounts.get(0));
    }

    @Test(expected = AccountLookupException.class)
    public void shouldThrowExceptionIfDBErrorOccursWhenListAccounts() throws SQLException {
        //given
        when(accountDao.listAccounts()).thenThrow(SQLException.class);

        //when
        accountService.listAccounts();
    }

    @Test
    public void shouldDeleteAccount() throws SQLException {
        //given
        String id = "1";

        //when
        accountService.deleteAccount(id);

        //then
        verify(accountDao).deleteAccount(id);
    }

    @Test(expected = AccountLookupException.class)
    public void shouldThrowExceptionIfDBErrorOccursWhenDeleteAccount() throws SQLException {
        //given
        String id = "1";
        doThrow(SQLException.class).when(accountDao).deleteAccount(id);

        //when
        accountService.deleteAccount(id);
    }
}