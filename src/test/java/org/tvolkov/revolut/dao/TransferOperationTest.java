package org.tvolkov.revolut.dao;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.tvolkov.revolut.database.DataSourceProvider;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

import static org.mockito.Mockito.*;
import static org.tvolkov.revolut.dao.TransferOperation.SELECT_FOR_UPDATE;
import static org.tvolkov.revolut.dao.TransferOperation.UPDATE;

@RunWith(MockitoJUnitRunner.class)
public class TransferOperationTest {

    @Mock
    private DataSourceProvider dataSourceProvider;

    @InjectMocks
    private TransferOperation transferOperation;

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEIfSourceAccountIsNull() throws InsufficientFundsException, AccountNotFoundException, SQLException {
        //when
        transferOperation.performTransfer(null, "222", null);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNPEIfDestinationAccountIsNull() throws InsufficientFundsException, AccountNotFoundException, SQLException {
        //when
        transferOperation.performTransfer("111", null, null);
    }

    @Test(expected = SQLException.class)
    public void shouldThrowExceptionIfThrownFromGetConnection() throws SQLException, InsufficientFundsException, AccountNotFoundException {
        //given
        DataSource dataSource = mock(DataSource.class);
        when(dataSource.getConnection()).thenThrow(SQLException.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);

        //when
        transferOperation.performTransfer("111", "222", null);
    }

    @Test(expected = SQLException.class)
    public void shouldThrowExceptionIfSetAutoCommitFails() throws SQLException, InsufficientFundsException, AccountNotFoundException {
        //given
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        doThrow(SQLException.class).when(connection).setAutoCommit(false);


        //when
        transferOperation.performTransfer("111", "222", null);
    }

    @Test(expected = SQLException.class)
    public void shouldThrowExceptionIfPrepareLockStatementFails() throws SQLException, InsufficientFundsException, AccountNotFoundException {
        //given
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(SELECT_FOR_UPDATE)).thenThrow(SQLException.class);


        //when
        transferOperation.performTransfer("123", "456", new BigDecimal("1.2"));
    }

    @Test(expected = SQLException.class)
    public void shouldThrowExceptionIfSetStringFailsOnPreparedStatement() throws SQLException, InsufficientFundsException, AccountNotFoundException {
        //given
        String srcAccNum = "123";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement lockStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(SELECT_FOR_UPDATE)).thenReturn(lockStatement);
        doThrow(SQLException.class).when(lockStatement).setString(1, srcAccNum);

        //when
        transferOperation.performTransfer(srcAccNum, "456", new BigDecimal("1.2"));
    }

    @Test(expected = SQLException.class)
    public void shouldThrowExceptionIfExecuteLockQueryFails() throws InsufficientFundsException, AccountNotFoundException, SQLException {
        //given
        String srcAccNum = "123";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement lockStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(SELECT_FOR_UPDATE)).thenReturn(lockStatement);
        when(lockStatement.executeQuery()).thenThrow(SQLException.class);

        //when
        transferOperation.performTransfer(srcAccNum, "456", new BigDecimal("1.2"));
    }

    @Test(expected = AccountNotFoundException.class)
    public void shouldThrowExceptionIfLockQueryReturnNoAccount() throws SQLException, InsufficientFundsException, AccountNotFoundException {
        //given
        String srcAccNum = "123";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement lockStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(SELECT_FOR_UPDATE)).thenReturn(lockStatement);
        ResultSet resultSet = mock(ResultSet.class);
        when(lockStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(false);

        //when
        transferOperation.performTransfer(srcAccNum, "456", new BigDecimal("1.2"));
    }

    @Test(expected = InsufficientFundsException.class)
    public void shouldThrowExceptionIfInsufficientFundsAtSrcAccount() throws SQLException, InsufficientFundsException, AccountNotFoundException {
        //given
        String srcAccNum = "123";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement lockStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(SELECT_FOR_UPDATE)).thenReturn(lockStatement);
        ResultSet resultSet = mock(ResultSet.class);
        when(lockStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getLong("id")).thenReturn(1L);
        when(resultSet.getBigDecimal("balance")).thenReturn(new BigDecimal("10.0"));
        when(resultSet.getString("number")).thenReturn("123");

        //when
        transferOperation.performTransfer(srcAccNum, "456", new BigDecimal("100.2"));
    }

    @Test(expected = SQLException.class)
    public void shouldThrowExceptionIfExecuteBatchFails() throws SQLException, InsufficientFundsException, AccountNotFoundException {
        //given
        String srcAccNum = "123";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement lockStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(SELECT_FOR_UPDATE)).thenReturn(lockStatement);
        ResultSet resultSet = mock(ResultSet.class);
        when(lockStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getLong("id")).thenReturn(1L);
        when(resultSet.getBigDecimal("balance")).thenReturn(new BigDecimal("1000.0"));
        when(resultSet.getString("number")).thenReturn("123");
        PreparedStatement updateStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(UPDATE)).thenReturn(updateStatement);
        when(updateStatement.executeBatch()).thenThrow(SQLException.class);

        //when
        transferOperation.performTransfer(srcAccNum, "456", new BigDecimal("100.2"));
    }

    @Test(expected = SQLException.class)
    public void shouldThrowExceptionIfExecuteBatchReturnsLessThatTwoUpdatedRows() throws SQLException, InsufficientFundsException, AccountNotFoundException {
        //given
        String srcAccNum = "123";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement lockStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(SELECT_FOR_UPDATE)).thenReturn(lockStatement);
        ResultSet resultSet = mock(ResultSet.class);
        when(lockStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getLong("id")).thenReturn(1L);
        when(resultSet.getBigDecimal("balance")).thenReturn(new BigDecimal("1000.0"));
        when(resultSet.getString("number")).thenReturn("123");
        PreparedStatement updateStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(UPDATE)).thenReturn(updateStatement);
        when(updateStatement.executeBatch()).thenReturn(new int[]{1});

        //when
        transferOperation.performTransfer(srcAccNum, "456", new BigDecimal("100.2"));
    }

    @Test
    public void shouldCommitTransactionIfNoErrors() throws SQLException, InsufficientFundsException, AccountNotFoundException {
        //given
        String srcAccNum = "123";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement lockStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(SELECT_FOR_UPDATE)).thenReturn(lockStatement);
        ResultSet resultSet = mock(ResultSet.class);
        when(lockStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getLong("id")).thenReturn(1L);
        when(resultSet.getBigDecimal("balance")).thenReturn(new BigDecimal("1000.0"));
        when(resultSet.getString("number")).thenReturn("123");
        PreparedStatement updateStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(UPDATE)).thenReturn(updateStatement);
        when(updateStatement.executeBatch()).thenReturn(new int[]{1, 1});

        //when
        transferOperation.performTransfer(srcAccNum, "456", new BigDecimal("100.2"));

        //then
        verify(connection).commit();
    }

    @Test
    public void shouldSynchronizeOnAccountNumbersInTheSameOrder() throws SQLException, InsufficientFundsException, AccountNotFoundException {
        //given
        String sourceAccountNumber = "123";
        String destinationAccountNumber = "456";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement lockStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(SELECT_FOR_UPDATE)).thenReturn(lockStatement);
        ResultSet resultSet = mock(ResultSet.class);
        when(lockStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getLong("id")).thenReturn(1L);
        when(resultSet.getBigDecimal("balance")).thenReturn(new BigDecimal("1000.0"));
        when(resultSet.getString("number")).thenReturn("123");
        PreparedStatement updateStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(UPDATE)).thenReturn(updateStatement);
        when(updateStatement.executeBatch()).thenReturn(new int[]{1, 1});

        //when
        List<Future<?>> futures = new LinkedList<>();
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        for (int i = 0; i < 5; i++){
            futures.add(executorService.submit(() -> {
                try {
                    transferOperation.performTransfer(sourceAccountNumber, destinationAccountNumber, new BigDecimal("100.00"));
                } catch (SQLException | InsufficientFundsException | AccountNotFoundException e) {
                    e.printStackTrace();
                }
            }));
        }

        for (int i = 0; i < 5; i++){
            futures.add(executorService.submit(() -> {
                try {
                    transferOperation.performTransfer(destinationAccountNumber, sourceAccountNumber, new BigDecimal("100.00"));
                } catch (SQLException | InsufficientFundsException | AccountNotFoundException e) {
                    e.printStackTrace();
                }
            }));
        }

        for (Future<?> transfer : futures){
            try {
                transfer.get();
            } catch (InterruptedException | ExecutionException e) {
                System.out.println("error while getting future result");
            }
        }

        //then
        verify(connection, times(10)).commit();
    }
}