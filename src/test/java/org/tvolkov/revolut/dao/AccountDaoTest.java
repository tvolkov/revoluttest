package org.tvolkov.revolut.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.tvolkov.revolut.database.DataSourceProvider;
import org.tvolkov.revolut.model.Account;
import org.tvolkov.revolut.rest.dto.AccountDto;
import org.tvolkov.revolut.rest.exceptions.NoAccountFoundException;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.*;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountDaoTest {

    @Mock
    private DataSourceProvider dataSourceProvider;

    @InjectMocks
    private AccountDao accountDao;

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionThrownFromDataSourceWhenFindingAccountById() throws SQLException, NoAccountFoundException {
        //given
        String accountId = "1";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenThrow(SQLException.class);

        //when
        accountDao.findByAccountId(accountId);
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionThrownFromDataSourceWhenFindingAccountByNumber() throws SQLException, NoAccountFoundException {
        //given
        String accountNumber = "1";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenThrow(SQLException.class);

        //when
        accountDao.findByAccountNumber(accountNumber);
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionFromPrepareStatementWhenFindingAccountById() throws SQLException, NoAccountFoundException {
        //given
        String accountId = "1";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);

        //when
        accountDao.findByAccountId(accountId);
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionFromPrepareStatementWhenFindingAccountByNumber() throws SQLException, NoAccountFoundException {
        //given
        String accountNumber = "1";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);

        //when
        accountDao.findByAccountNumber(accountNumber);
    }

    //
    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionFromSetStringWhenFindingAccountById() throws SQLException, NoAccountFoundException {
        //given
        String accountId = "1";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        doThrow(SQLException.class).when(preparedStatement).setString(anyInt(), any());

        //when
        accountDao.findByAccountId(accountId);
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionFromSetStringWhenFindingAccountByNumber() throws SQLException, NoAccountFoundException {
        //given
        String accountNumber = "1";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        doThrow(SQLException.class).when(preparedStatement).setString(anyInt(), any());

        //when
        accountDao.findByAccountNumber(accountNumber);
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionFromExecuteQueryWhenFindingAccountById() throws SQLException, NoAccountFoundException {
        //given
        String accountId = "1";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        doNothing().when(preparedStatement).setString(anyInt(), any());
        when(preparedStatement.executeQuery()).thenThrow(SQLException.class);

        //when
        accountDao.findByAccountId(accountId);
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionFromExecuteQueryWhenFindingAccountByNumber() throws SQLException, NoAccountFoundException {
        //given
        String accountNumber = "1";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        doNothing().when(preparedStatement).setString(anyInt(), any());
        when(preparedStatement.executeQuery()).thenThrow(SQLException.class);

        //when
        accountDao.findByAccountNumber(accountNumber);
    }

    @Test(expected = NoAccountFoundException.class)
    public void shouldThrowExceptionWhenEmptyResultSetReturnedWhenFindingAccountById() throws SQLException, NoAccountFoundException {
        //given
        String accountId = "1";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        doNothing().when(preparedStatement).setString(anyInt(), any());
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.isBeforeFirst()).thenReturn(false);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        //when
        accountDao.findByAccountId(accountId);
    }

    @Test(expected = NoAccountFoundException.class)
    public void shouldThrowExceptionWhenEmptyResultSetReturnedWhenFindingAccountByNumber() throws SQLException, NoAccountFoundException {
        //given
        String accountNumber = "1";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        doNothing().when(preparedStatement).setString(anyInt(), any());
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.isBeforeFirst()).thenReturn(false);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);

        //when
        accountDao.findByAccountNumber(accountNumber);
    }

    @Test
    public void shouldReturnFirstResultFromResultSetWhenFindingAccountById() throws SQLException, NoAccountFoundException {
        //given
        long accId = 1L;
        String accountId = String.valueOf(accId);
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        doNothing().when(preparedStatement).setString(anyInt(), any());
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.isBeforeFirst()).thenReturn(true);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.getLong("id")).thenReturn(accId);
        when(resultSet.getString("balance")).thenReturn("100");
        when(resultSet.getString("number")).thenReturn("123");

        //when
        Account account = accountDao.findByAccountId(accountId);

        //then
        assertNotNull(account);
        assertEquals(accId, account.getId());
        assertEquals("100", account.getBalance().toString());
        assertEquals("123", account.getNumber());
    }

    @Test
    public void shouldReturnFirstResultFromResultSetWhenFindingAccountByNumber() throws SQLException, NoAccountFoundException {
        //given
        String accountNumber = "1234";
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        Connection connection = mock(Connection.class);
        when(dataSource.getConnection()).thenReturn(connection);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        doNothing().when(preparedStatement).setString(anyInt(), any());
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.isBeforeFirst()).thenReturn(true);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.getLong("id")).thenReturn(1L);
        when(resultSet.getString("balance")).thenReturn("100");
        when(resultSet.getString("number")).thenReturn(accountNumber);

        //when
        Account account = accountDao.findByAccountNumber(accountNumber);

        //then
        assertNotNull(account);
        assertEquals(1L, account.getId());
        assertEquals("100", account.getBalance().toString());
        assertEquals(accountNumber, account.getNumber());
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionFromGetConnectionWhenCreateAccount() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenThrow(SQLException.class);

        //when
        accountDao.createAccount(null);
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionFromPrepareStatementWhenCreateAccount() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString(), anyInt())).thenThrow(SQLException.class);

        //when
        accountDao.createAccount(null);
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionFromSetBigDecimalWhenCreateAccount() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString(), anyInt())).thenReturn(preparedStatement);
        doThrow(SQLException.class).when(preparedStatement).setBigDecimal(anyInt(), any(BigDecimal.class));

        //when
        accountDao.createAccount(new AccountDto("100.00", "1234"));
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionFromSetStringWhenCreateAccount() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString(), anyInt())).thenReturn(preparedStatement);
        doThrow(SQLException.class).when(preparedStatement).setString(anyInt(), anyString());

        //when
        accountDao.createAccount(new AccountDto("100.00", "1234"));

    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionFromExecuteUpdateWhenCreateAccount() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString(), anyInt())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenThrow(SQLException.class);

        //when
        accountDao.createAccount(new AccountDto("100.00", "1234"));
    }

    @Test(expected = SQLException.class)
    public void shouldThrowSQLExceptionIfNoRowsWereAffectedWhenCreateAccount() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString(), anyInt())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(0);

        //when
        accountDao.createAccount(new AccountDto("100.00", "1234"));
    }

    @Test
    public void shouldReturnIdOfCreatedAccount() throws SQLException {
        //given
        long expectedId = 1L;
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString(), anyInt())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(1);
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next()).thenReturn(true);
        when(resultSet.getLong(1)).thenReturn(expectedId);
        when(preparedStatement.getGeneratedKeys()).thenReturn(resultSet);

        //when
        long actualId = accountDao.createAccount(new AccountDto("100.00", "1234"));

        //then
        assertEquals(expectedId, actualId);
    }

    @Test(expected = SQLException.class)
    public void shouldThrowSQLExceptionIfNoIdIsAvailableWhenCreateAccount() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString(), anyInt())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(1);
        ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next()).thenReturn(false);
        when(preparedStatement.getGeneratedKeys()).thenReturn(resultSet);

        //when
        accountDao.createAccount(new AccountDto("100.00", "1234"));
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionFromGetConnectionWhenListAccounts() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenThrow(SQLException.class);

        //when
        accountDao.listAccounts();
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptoinFromCreateStatementWhenListAccounts() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.createStatement()).thenThrow(SQLException.class);

        //when
        accountDao.listAccounts();
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLEXceptionFromExecuteQueryWhenListAccounts() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        Statement statement = mock(Statement.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenThrow(SQLException.class);

        //when
        accountDao.listAccounts();
    }

    @Test
    public void shouldReturnListOfAccountsWhenListAccounts() throws SQLException {
        //given
        Account account = new Account(1L, new BigDecimal("100.00"), "1234");
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        Statement statement = mock(Statement.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.createStatement()).thenReturn(statement);
        ResultSet resultSet = mock(ResultSet.class);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(false);
        when(resultSet.getLong("id")).thenReturn(account.getId());
        when(resultSet.getBigDecimal("balance")).thenReturn(account.getBalance());
        when(resultSet.getString("number")).thenReturn(account.getNumber());

        //when
        List<Account> accountList = accountDao.listAccounts();

        //then
        assertEquals(1, accountList.size());
        assertEquals(account, accountList.get(0));
    }

    @Test
    public void shouldReturnEmptyListIfThereAreNoAccountsWhenListAccounts() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        Statement statement = mock(Statement.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.createStatement()).thenReturn(statement);
        ResultSet resultSet = mock(ResultSet.class);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(false);

        //when
        List<Account> accountList = accountDao.listAccounts();

        //then
        assertNotNull(accountList);
        assertEquals(0, accountList.size());
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionFromGetConnectionWhenDeleteAccount() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenThrow(SQLException.class);

        //when
        accountDao.deleteAccount(null);
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionFromPrepareStatementWhenDeleteAccount() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenThrow(SQLException.class);

        //when
        accountDao.deleteAccount(null);
    }

    @Test(expected = SQLException.class)
    public void shouldRethrowSQLExceptionsFromSetLongWhenDeleteAccount() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        doThrow(SQLException.class).when(preparedStatement).setLong(1, 1L);

        //when
        accountDao.deleteAccount("1");
    }

    @Test(expected = SQLException.class)
    public void shouldThrowSQLExceptionIfNoRowsWhereAffectedWhenDeleteAccount() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(0);

        //when
        accountDao.deleteAccount("1");
    }

    @Test
    public void shouldDeleteAccount() throws SQLException {
        //given
        DataSource dataSource = mock(DataSource.class);
        Connection connection = mock(Connection.class);
        PreparedStatement preparedStatement = mock(PreparedStatement.class);
        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(dataSource.getConnection()).thenReturn(connection);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeUpdate()).thenReturn(1);

        //when
        accountDao.deleteAccount("1");
    }
}