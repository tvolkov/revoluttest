package org.tvolkov.revolut.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;

@AllArgsConstructor
@EqualsAndHashCode
public class Account {

    @Getter
    private long id;

    @Getter
    private BigDecimal balance;

    @Getter
    private String number;
}
