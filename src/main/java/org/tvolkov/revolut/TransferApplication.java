package org.tvolkov.revolut;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tvolkov.revolut.database.DataSourceProvider;
import org.tvolkov.revolut.database.PooledDataSourceProvider;
import org.tvolkov.revolut.database.ScriptRunner;
import org.tvolkov.revolut.properties.PropertiesHolder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TransferApplication {

    private static final int THREAD_POOL_SIZE = 2;
    private static final Logger logger = LoggerFactory.getLogger(TransferApplication.class);

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        executorService.submit(TransferApplication::startJetty);
        executorService.submit(TransferApplication::startDatabase);
    }

    private static void startDatabase() {
        try {
            org.h2.tools.Server.createTcpServer().start();
        } catch (SQLException e) {
            logger.error("Exception occurred in database: ", e);
            System.exit(2);
        }

        try {
            initDatabase();
        } catch (SQLException | IOException e) {
            logger.error("Unable to initialize database", e);
            System.exit(3);
        }
    }

    private static void initDatabase() throws SQLException, IOException {
        DataSourceProvider basicDataSourceProvider = new PooledDataSourceProvider(new PropertiesHolder());
        ScriptRunner scriptRunner = new ScriptRunner(basicDataSourceProvider.getDataSource().getConnection(), false, false);
        String file = "src/main/resources/init.sql";
        scriptRunner.runScript(new BufferedReader(new FileReader(file)));
    }

    private static void startJetty() {
        Server server = new Server();
        ServletContextHandler servletContextHandler = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
        servletContextHandler.setContextPath("/");
        server.setHandler(servletContextHandler);

        ServerConnector serverConnector = new ServerConnector(server);
        PropertiesHolder propertiesHolder = new PropertiesHolder();
        serverConnector.setAcceptQueueSize(propertiesHolder.getPropertyAsInt("jetty.queue.size"));
        serverConnector.setPort(propertiesHolder.getPropertyAsInt("http.port"));
        server.setConnectors(new Connector[]{serverConnector});

        ServletHolder servletHolder = servletContextHandler.addServlet(ServletContainer.class, "/api/*");
        servletHolder.setInitOrder(0);
        servletHolder.setInitParameter("javax.ws.rs.Application", "org.tvolkov.revolut.ApplicationConfig");

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            logger.error("Jetty exited with exception: ", e);
            System.exit(1);
        } finally {
            server.destroy();
        }
    }
}

