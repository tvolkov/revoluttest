package org.tvolkov.revolut.service;

import org.tvolkov.revolut.dao.AccountDao;
import org.tvolkov.revolut.model.Account;
import org.tvolkov.revolut.rest.dto.AccountDto;
import org.tvolkov.revolut.rest.exceptions.AccountLookupException;
import org.tvolkov.revolut.rest.exceptions.NoAccountFoundException;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.List;

@Singleton
public class AccountService {

    private AccountDao accountDao;

    private AccountDtoValidator accountDtoValidator;

    @Inject
    public AccountService(AccountDao accountDao, AccountDtoValidator accountDtoValidator) {
        this.accountDao = accountDao;
        this.accountDtoValidator = accountDtoValidator;
    }

    public Account getAccount(String accountId) {
        try {
            return accountDao.findByAccountId(accountId);
        } catch (SQLException e) {
            throw new AccountLookupException(e, Response.Status.INTERNAL_SERVER_ERROR);
        } catch (NoAccountFoundException e1) {
            throw new AccountLookupException(e1, Response.Status.NOT_FOUND);
        }
    }

    public long createAccount(AccountDto accountDto) {
        accountDtoValidator.validateAccountDto(accountDto);

        try {
            return accountDao.createAccount(accountDto);
        } catch (SQLException e) {
            throw new AccountLookupException(e, Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    public List<Account> listAccounts() {
        try {
            return accountDao.listAccounts();
        }  catch (SQLException e) {
            throw new AccountLookupException(e, Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    public void deleteAccount(String accountId) {
        try {
            accountDao.deleteAccount(accountId);
        } catch (SQLException e) {
            throw new AccountLookupException(e, Response.Status.INTERNAL_SERVER_ERROR);
        }
    }
}
