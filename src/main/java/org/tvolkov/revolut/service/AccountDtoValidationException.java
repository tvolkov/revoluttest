package org.tvolkov.revolut.service;

import lombok.Getter;

import javax.ws.rs.core.Response;

public class AccountDtoValidationException extends RuntimeException {

    @Getter
    private Response.Status status;

    public AccountDtoValidationException(String msg){
        super(msg);
        this.status = Response.Status.BAD_REQUEST;
    }
}
