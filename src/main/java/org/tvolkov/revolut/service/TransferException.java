package org.tvolkov.revolut.service;

import lombok.Getter;
import org.tvolkov.revolut.rest.exceptions.TransferDataValidationException;
import org.tvolkov.revolut.rest.dto.TransferResult;

import javax.ws.rs.core.Response;

public class TransferException extends RuntimeException {

    @Getter
    private Response.Status status;

    @Getter
    private TransferResult transferResult;

    TransferException(TransferDataValidationException e, Response.Status status, TransferResult transferResult) {
        super(e);
        this.status = status;
        this.transferResult = transferResult;
    }

    public TransferException(Response.Status status, TransferResult transferResult){
        super();
        this.status = status;
        this.transferResult = transferResult;
    }
}
