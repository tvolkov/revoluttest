package org.tvolkov.revolut.service;

import org.eclipse.jetty.util.StringUtil;
import org.tvolkov.revolut.rest.dto.TransferData;
import org.tvolkov.revolut.rest.exceptions.TransferDataValidationException;

import javax.inject.Singleton;
import java.math.BigDecimal;

@Singleton
public class TransferDataValidator {

    public void validateTransferData(TransferData transferData) throws TransferDataValidationException {
        BigDecimal amount = transferData.getAmount();
        if (amount == null || amount.compareTo(BigDecimal.ZERO) < 0)
            throw new TransferDataValidationException("Amount to transfer should be greater than zero");

        if (StringUtil.isBlank(transferData.getFrom()))
            throw new TransferDataValidationException("Source account should be specified");

        if (StringUtil.isBlank(transferData.getTo()))
            throw new TransferDataValidationException("Destination account should be specified");
    }
}
