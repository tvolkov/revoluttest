package org.tvolkov.revolut.service;

import org.eclipse.jetty.util.StringUtil;
import org.tvolkov.revolut.rest.dto.AccountDto;

import javax.inject.Singleton;
import java.math.BigDecimal;

@Singleton
public class AccountDtoValidator {

    void validateAccountDto(AccountDto accountDto){
        if (accountDto == null)
            throw new AccountDtoValidationException("account dto is null");

        if (StringUtil.isBlank(accountDto.getAmount()))
            throw new AccountDtoValidationException("account's amount is empty");
        else if (new BigDecimal(accountDto.getAmount()).compareTo(BigDecimal.ZERO) < 0)
            throw new AccountDtoValidationException("amount can not be negative");

        if (StringUtil.isBlank(accountDto.getNumber()))
            throw new AccountDtoValidationException("account number should be specified");
    }
}
