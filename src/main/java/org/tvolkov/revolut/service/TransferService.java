package org.tvolkov.revolut.service;

import org.tvolkov.revolut.dao.AccountNotFoundException;
import org.tvolkov.revolut.dao.InsufficientFundsException;
import org.tvolkov.revolut.dao.TransferOperation;
import org.tvolkov.revolut.rest.dto.TransferData;
import org.tvolkov.revolut.rest.dto.TransferResult;
import org.tvolkov.revolut.rest.exceptions.TransferDataValidationException;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Singleton
public class TransferService {

    private TransferDataValidator transferDataValidator;
    private TransferOperation transferOperation;
    private Lock transactionLock = new ReentrantLock();

    @Inject
    public TransferService(TransferDataValidator transferDataValidator, TransferOperation transferOperation){
        this.transferDataValidator = transferDataValidator;
        this.transferOperation = transferOperation;
    }

    public TransferResult transfer(TransferData transferData) {
        validateTransferData(transferData);
        performTransfer(transferData);
        return createSuccessfulResult(transferData);
    }

    private void performTransfer(TransferData transferData) {
        transactionLock.lock();

        try {
            transferOperation.performTransfer(transferData.getFrom(), transferData.getTo(), transferData.getAmount());
        } catch (SQLException e) {
            throw new TransferException(Response.Status.INTERNAL_SERVER_ERROR, createFailedResult("Error while executing transfer", transferData));
        } catch (InsufficientFundsException e) {
            throw new TransferException(Response.Status.NOT_ACCEPTABLE, createFailedResult("Insufficient funds",transferData));
        } catch (AccountNotFoundException e) {
            throw new TransferException(Response.Status.BAD_REQUEST, createFailedResult(e.getMessage(), transferData));
        } finally {
            transactionLock.unlock();
        }
    }

    private void validateTransferData(TransferData transferData) {
        try {
            transferDataValidator.validateTransferData(transferData);
        } catch (TransferDataValidationException e) {
            throw new TransferException(e, Response.Status.BAD_REQUEST, createFailedResult(e.getMessage(),transferData));
        }
    }

    private TransferResult createFailedResult(String message, TransferData transferData){
        return createResult(false, message,transferData);
    }

    private TransferResult createSuccessfulResult(TransferData transferData){
        return createResult(true, null,transferData);
    }

    private TransferResult createResult(boolean success, String message, TransferData transferData){
        return new TransferResult(success, message, transferData.getAmount().toString(), transferData.getFrom(), transferData.getTo());
    }

}
