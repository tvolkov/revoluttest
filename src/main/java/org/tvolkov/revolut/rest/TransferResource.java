package org.tvolkov.revolut.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tvolkov.revolut.rest.dto.TransferData;
import org.tvolkov.revolut.service.TransferService;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Singleton
@Path("/transfer")
@Produces(APPLICATION_JSON)
public class TransferResource {

    private static Logger logger = LoggerFactory.getLogger(TransferResource.class);

    private TransferService transferService;

    @Inject
    public TransferResource(TransferService transferService) {
        this.transferService = transferService;
    }

    @POST
    @Consumes(APPLICATION_JSON)
    public Response transferFunds(TransferData transferData) {
        logger.debug("executing transfer: from " + transferData.getFrom() + ", to: " + transferData.getTo() + ", amount: " + transferData.getAmount());
        return Response.ok().entity(transferService.transfer(transferData)).build();
    }
}
