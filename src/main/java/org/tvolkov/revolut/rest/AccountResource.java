package org.tvolkov.revolut.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tvolkov.revolut.rest.dto.AccountDto;
import org.tvolkov.revolut.service.AccountService;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Singleton
@Path("/account")
@Produces(APPLICATION_JSON)
public class AccountResource {

    private static final Logger logger = LoggerFactory.getLogger(AccountResource.class);

    private AccountService accountService;

    @Inject
    public AccountResource(AccountService accountService) {
        this.accountService = accountService;
    }

    @GET
    @Path("/{id}")
    public Response getAccount(@PathParam("id") String accountId) {
        return Response.ok().entity(accountService.getAccount(accountId)).build();
    }

    @GET
    public Response listAccounts(){
        return Response.ok().entity(accountService.listAccounts()).build();
    }

    @POST
    @Consumes(APPLICATION_JSON)
    public Response createAccount(AccountDto accountDto, @Context UriInfo uriInfo){
        long newAccountId = accountService.createAccount(accountDto);
        UriBuilder uriBuilder = uriInfo.getAbsolutePathBuilder();
        uriBuilder.path(Long.toString(newAccountId));
        return Response.created(uriBuilder.build()).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteAccount(@PathParam("id") String accountId){
        logger.debug("deleting account with id {}", accountId);
        accountService.deleteAccount(accountId);
        return Response.noContent().build();
    }
}
