package org.tvolkov.revolut.rest.providers;

import org.tvolkov.revolut.service.AccountDtoValidationException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class AccountDtoValidationExceptionMapper implements ExceptionMapper<AccountDtoValidationException> {
    @Override
    public Response toResponse(AccountDtoValidationException exception) {
        return Response.status(exception.getStatus()).entity(exception.getMessage()).build();
    }
}
