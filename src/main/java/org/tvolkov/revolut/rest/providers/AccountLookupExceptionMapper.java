package org.tvolkov.revolut.rest.providers;

import org.tvolkov.revolut.rest.exceptions.AccountLookupException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class AccountLookupExceptionMapper implements ExceptionMapper<AccountLookupException> {

    @Override
    public Response toResponse(AccountLookupException exception) {
        return Response.status(exception.getStatus()).entity(exception.getCause().getMessage()).build();
    }
}
