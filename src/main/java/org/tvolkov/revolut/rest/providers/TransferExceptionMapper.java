package org.tvolkov.revolut.rest.providers;

import org.tvolkov.revolut.service.TransferException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class TransferExceptionMapper implements ExceptionMapper<TransferException> {

    @Override
    public Response toResponse(TransferException exception) {
        return Response.status(exception.getStatus()).entity(exception.getTransferResult()).build();
    }
}
