package org.tvolkov.revolut.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor
@AllArgsConstructor
public class TransferData implements Serializable {

    @XmlElement(name = "amount")
    @Getter
    @Setter
    private BigDecimal amount;

    @XmlElement(name = "from")
    @Getter
    @Setter
    private String from;

    @XmlElement(name = "to")
    @Getter
    @Setter
    private String to;
}
