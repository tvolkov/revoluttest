package org.tvolkov.revolut.rest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class TransferResult implements Serializable {

    @Getter
    @Setter
    private boolean success;

    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;

    @Getter
    @Setter
    private String amount;

    @Getter
    @Setter
    private String fromAcc;

    @Getter
    @Setter
    private String toAcc;
}
