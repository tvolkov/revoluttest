package org.tvolkov.revolut.rest.exceptions;

import lombok.Getter;

import javax.ws.rs.core.Response;

public class AccountLookupException extends RuntimeException {

    @Getter
    private Response.Status status;

    public AccountLookupException(Exception e, Response.Status status){
        super(e);
        this.status = status;
    }
}
