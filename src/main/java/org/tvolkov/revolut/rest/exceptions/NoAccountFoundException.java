package org.tvolkov.revolut.rest.exceptions;

public class NoAccountFoundException extends Exception {
    public NoAccountFoundException(String msg) {
        super(msg);
    }
}
