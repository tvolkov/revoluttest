package org.tvolkov.revolut.rest.exceptions;

public class TransferDataValidationException extends Exception {
    public TransferDataValidationException(String msg) {
        super(msg);
    }
}
