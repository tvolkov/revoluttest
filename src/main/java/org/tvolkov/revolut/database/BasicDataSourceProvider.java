package org.tvolkov.revolut.database;

import org.apache.commons.dbcp2.BasicDataSource;
import org.tvolkov.revolut.properties.PropertiesHolder;

import javax.sql.DataSource;

public class BasicDataSourceProvider implements DataSourceProvider {

    private static String JDBC_DRIVER = "jdbc.driver";
    private static String CONNECTION_URI = "db.connection.uri";
    private static String USERNAME = "db.user";
    private static String PASSWORD = "db.password";

    private PropertiesHolder propertiesHolder;
    private DataSource dataSource;

    public BasicDataSourceProvider(PropertiesHolder propertiesHolder){
        this.propertiesHolder = propertiesHolder;
        initConnectionPool();
    }

    private void initConnectionPool() {
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName(propertiesHolder.getProperty(JDBC_DRIVER));
        basicDataSource.setUrl(propertiesHolder.getProperty(CONNECTION_URI));
        basicDataSource.setUsername(propertiesHolder.getProperty(USERNAME));
        basicDataSource.setPassword(propertiesHolder.getProperty(PASSWORD));
        this.dataSource = basicDataSource;
    }

    @Override
    public DataSource getDataSource() {
        return dataSource;
    }
}
