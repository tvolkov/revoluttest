package org.tvolkov.revolut.database;

import javax.sql.DataSource;

public interface DataSourceProvider {
    DataSource getDataSource();
}
