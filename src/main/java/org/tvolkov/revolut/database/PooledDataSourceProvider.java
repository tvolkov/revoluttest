package org.tvolkov.revolut.database;

import org.apache.commons.dbcp2.*;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.tvolkov.revolut.properties.PropertiesHolder;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.sql.DataSource;

@Singleton
public class PooledDataSourceProvider implements DataSourceProvider {

    private static final String CONNECTION_URI = "db.connection.uri";

    private PropertiesHolder propertiesHolder;
    private DataSource dataSource;

    @Inject
    public PooledDataSourceProvider(PropertiesHolder propertiesHolder) {
        this.propertiesHolder = propertiesHolder;
        initDataSource();
    }

    private void initDataSource() {
        ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(propertiesHolder.getProperty(CONNECTION_URI));
        PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory, null);
        ObjectPool<PoolableConnection> connectionPool = new GenericObjectPool<>(poolableConnectionFactory);
        poolableConnectionFactory.setPool(connectionPool);
        this.dataSource = new PoolingDataSource<>(connectionPool);
    }

    @Override
    public DataSource getDataSource() {
        return dataSource;
    }
}
