package org.tvolkov.revolut.properties;

import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Singleton
public class PropertiesHolder {
    private static final String APPLICATION_PROPERTIES = "application.properties";

    private Properties properties = new Properties();

    public PropertiesHolder(){
        loadProperties();
    }

    public String getProperty(String key){
        return properties.getProperty(key);
    }

    public int getPropertyAsInt(String key) {
        return Integer.parseInt(properties.getProperty(key));
    }

    private void loadProperties() {
        String pathToFile = System.getProperty("app.property.file");
        if (pathToFile == null) loadDefaultProperties();
        else loadPropertiesFromPath(pathToFile);
    }

    private void loadPropertiesFromPath(String pathToFile) {
        loadPropertiesFromClasspathResource(pathToFile);
    }

    private void loadDefaultProperties() {
        loadPropertiesFromClasspathResource(APPLICATION_PROPERTIES);
    }

    private void loadPropertiesFromClasspathResource(String resourceName){
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(resourceName);
        try {
            properties.load(inputStream);
        } catch (IOException e) {
            if (!resourceName.equals(APPLICATION_PROPERTIES)) loadDefaultProperties();
            else throw new RuntimeException("Unable to read properties from resource " + resourceName);
        }
    }
}
