package org.tvolkov.revolut;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.tvolkov.revolut.dao.AccountDao;
import org.tvolkov.revolut.dao.TransferOperation;
import org.tvolkov.revolut.database.DataSourceProvider;
import org.tvolkov.revolut.database.PooledDataSourceProvider;
import org.tvolkov.revolut.properties.PropertiesHolder;
import org.tvolkov.revolut.service.*;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("")
public class ApplicationConfig extends ResourceConfig {

    public ApplicationConfig() {

        packages("org.tvolkov.revolut.rest", "com.fasterxml.jackson.jaxrs.json");

        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bind(PropertiesHolder.class).to(PropertiesHolder.class);
                bind(AccountDtoValidator.class).to(AccountDtoValidator.class);
                bind(TransferDataValidator.class).to(TransferDataValidator.class);
                bind(PooledDataSourceProvider.class).to(DataSourceProvider.class);
                bind(AccountService.class).to(AccountService.class);
                bind(AccountDao.class).to(AccountDao.class);
                bind(TransferOperation.class).to(TransferOperation.class);
                bind(TransferService.class).to(TransferService.class);
            }
        });
    }
}
