package org.tvolkov.revolut.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tvolkov.revolut.database.DataSourceProvider;
import org.tvolkov.revolut.model.Account;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Singleton
public class TransferOperation {

    private static Logger logger = LoggerFactory.getLogger(TransferOperation.class);

    static final String SELECT_FOR_UPDATE = "select * from account where number = ? for update";
    static final String UPDATE = "update account set balance = ? where id = ?";

    private DataSourceProvider dataSourceProvider;

    @Inject
    public TransferOperation(DataSourceProvider dataSourceProvider) {
        this.dataSourceProvider = dataSourceProvider;
    }

    public void performTransfer(String sourceAccountNumber, String destinationAccountNumber, BigDecimal amountToTransfer) throws SQLException, InsufficientFundsException, AccountNotFoundException {
        String firstAccountToSyncOn, secondAccountToSyncOn;
        if (sourceAccountNumber.compareTo(destinationAccountNumber) < 0){
            firstAccountToSyncOn = sourceAccountNumber;
            secondAccountToSyncOn = destinationAccountNumber;
        } else {
            firstAccountToSyncOn = destinationAccountNumber;
            secondAccountToSyncOn = sourceAccountNumber;
        }

        synchronized (firstAccountToSyncOn){
            synchronized (secondAccountToSyncOn){
                executeActualTransfer(sourceAccountNumber, destinationAccountNumber, amountToTransfer);
            }
        }
    }

    private void executeActualTransfer(String sourceAccountNumber, String destinationAccountNumber, BigDecimal amountToTransfer) throws AccountNotFoundException, InsufficientFundsException, SQLException {
        Connection connection = null;

        Account srcAccount, dstAccount;
        try {
            connection = dataSourceProvider.getDataSource().getConnection();
            connection.setAutoCommit(false);

            srcAccount = getAccount(connection, sourceAccountNumber);
            dstAccount = getAccount(connection, destinationAccountNumber);

            BigDecimal srcAccNewAmount = srcAccount.getBalance().subtract(amountToTransfer);
            if (srcAccNewAmount.compareTo(BigDecimal.ZERO) < 0)
                throw new InsufficientFundsException("Insufficient funds at source account");

            try (PreparedStatement updateStatement = connection.prepareStatement(UPDATE)) {
                updateStatement.setBigDecimal(1, srcAccNewAmount);
                updateStatement.setLong(2, srcAccount.getId());
                updateStatement.addBatch();

                updateStatement.setBigDecimal(1, dstAccount.getBalance().add(amountToTransfer));
                updateStatement.setLong(2, dstAccount.getId());
                updateStatement.addBatch();

                int[] rowsUpdated = updateStatement.executeBatch();
                if (rowsUpdated.length != 2) throw new SQLException("error occurred when executing batch");
            }

            connection.commit();
        } catch (SQLException e) {
            logger.info("exception during transaction, rolling back ", e);
            if (connection != null) connection.rollback();
            throw e;
        } finally {
            if (connection != null) connection.close();
        }
    }

    private Account getAccount(Connection connection, String accountNumber) throws SQLException, AccountNotFoundException {
        try (PreparedStatement lockStatement = connection.prepareStatement(SELECT_FOR_UPDATE)) {
            lockStatement.setString(1, accountNumber);
            try (ResultSet resultSet = lockStatement.executeQuery()) {
                if (resultSet.next())
                    return new Account(resultSet.getLong("id"), resultSet.getBigDecimal("balance"), resultSet.getString("number"));
                else throw new AccountNotFoundException("Unable to find account with number " + accountNumber);
            }
        }
    }
}
