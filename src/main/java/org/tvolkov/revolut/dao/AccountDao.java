package org.tvolkov.revolut.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tvolkov.revolut.database.DataSourceProvider;
import org.tvolkov.revolut.model.Account;
import org.tvolkov.revolut.rest.dto.AccountDto;
import org.tvolkov.revolut.rest.exceptions.NoAccountFoundException;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class AccountDao {

    private static Logger logger = LoggerFactory.getLogger(AccountDao.class);

    private static final String FIND_ACCOUNT_BY_NUMBER = "select * from account where number = ?";
    private static final String FIND_ACCOUNT_BY_ID = "select * from account where id = ?";
    private static final String CREATE_ACCOUNT = "insert into account (balance, number) values (?, ?)";
    private static final String LIST_ACCOUNTS = "select * from account order by id";
    private static final String DELETE_ACCOUNT = "delete from account where id = ?";

    private DataSourceProvider dataSourceProvider;

    @Inject
    public AccountDao(DataSourceProvider dataSourceProvider) {
        this.dataSourceProvider = dataSourceProvider;
    }

    Account findByAccountNumber(String accountNumber) throws SQLException, NoAccountFoundException {
        logger.debug("getting account by number " + accountNumber);
        return findAccount(FIND_ACCOUNT_BY_NUMBER, accountNumber);
    }

    public Account findByAccountId(String accountId) throws SQLException, NoAccountFoundException {
        logger.debug("getting account by id " + accountId);
        return findAccount(FIND_ACCOUNT_BY_ID, accountId);
    }

    private Account findAccount(String sql, String param) throws SQLException, NoAccountFoundException {
        try (
                Connection connection = dataSourceProvider.getDataSource().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(sql)
        ) {
            preparedStatement.setString(1, param);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (!resultSet.isBeforeFirst()) throw new NoAccountFoundException("No account were found");
                resultSet.next();
                return new Account(resultSet.getLong("id"), new BigDecimal(resultSet.getString("balance")), resultSet.getString("number"));
            }

        }
    }

    public long createAccount(AccountDto accountDto) throws SQLException {
        try (
                Connection connection = dataSourceProvider.getDataSource().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(CREATE_ACCOUNT, Statement.RETURN_GENERATED_KEYS)
        ) {
            preparedStatement.setBigDecimal(1, new BigDecimal(accountDto.getAmount()));
            preparedStatement.setString(2, accountDto.getNumber());
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) throw new SQLException("creating account failed, no rows affected");
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                if (resultSet.next())
                    return resultSet.getLong(1);
                else throw new SQLException("creating account failed, no id available");
            }
        }
    }

    public List<Account> listAccounts() throws SQLException {
        try (
                Connection connection = dataSourceProvider.getDataSource().getConnection();
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery(LIST_ACCOUNTS)
        ) {
            List<Account> resultList = new ArrayList<>();
            while (resultSet.next()) {
                resultList.add(new Account(resultSet.getLong("id"), resultSet.getBigDecimal("balance"), resultSet.getString("number")));
            }

            return resultList;
        }
    }

    public void deleteAccount(String accountId) throws SQLException {
        try (
                Connection connection = dataSourceProvider.getDataSource().getConnection();
                PreparedStatement preparedStatement = connection.prepareStatement(DELETE_ACCOUNT)
        ) {
            preparedStatement.setLong(1, Long.valueOf(accountId));
            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) throw new SQLException("delete account failed, no rows affected");
        }
    }
}
