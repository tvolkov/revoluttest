drop table if exists account;
create memory temporary table if not exists account (
  id int primary key auto_increment not null,
  balance numeric(12, 2) not null,
  number varchar not null,
  constraint unique_number unique (number)
);

insert into account values (1, 1000.11, '222');
insert into account values (2, 9099987.35, '333');
insert into account values (3, 0.00, '444');
insert into account values (4, 150.00, '555');
