##To start the application, run
`./gradlew clean build && java -jar build\libs\revoluttest-1.0-SNAPSHOT-all.jar`

##General information

* Since I had the requirement not to use Spring, I decided to use Jersey, since it is also one of 
the most popular MVC frameworks. Also it supports standard JSR-330 depnedency injection, so I thought it's what I need.

* As a database I used H2 running in in-memory mode, and jetty as a servlet container. Both of them are started and initialized
within my application code, which is not perfect decision, however I've intentionally done it for the sake of simplicity. 

* I decided not to use ORM, rather JDBC, since in my opinion using any ORM framework for such a task would be an overkill, whereas plain old JDBC workd just fine.

* For the synchronization purposes I added the pessimistic lock in the TransferService (e.g. only ony transfer operation can run at one time), which in my opinion is 
a good choice for achieving the goals I had. I.e. it's ok for one request to exclusively update table in database, since there was no need for high concurrency

* The application doesn't support multiple currencies, I decided that using one 'default' currency would be enough.

* The ScriptRunner class was simply copied from github (I'm not the author of it). An easy way to run sql scripts.

* I used Lombok wherever possible to keep code clean

* I used junit/mockito for unit testing and also jersey-test for functional tests. There were no integration tests added

* The transfer method (see API description) could have been implemented in another way - by passing data in the request params, which is probably more natural for such purpose. 
But I intentionally pass the request data in the body since I wanted to yse POST method for this operation to indicate that the state of the system is changing. On the other hand, using POST with empty body would be misleading (although it's possible) and confusing, imho

## The API looks like follows:

**Create account**
----
  Create new account provided its number and initial amount

* **URL**

  /api/account

* **Method:**

  `POST`
  

* **Example Request Body**

  ```
     {
         "amount": 100.35,
         "number": "990"
     }
     ```
     
* **Required request header**
     
  `Content-Type: application/json`

* **Success Response:**

  * **Code:** 201 Created<br />
    **Header:** `Location →http://localhost:8080/api/account/5`
 
* **Error Response:**

  * **Code:** 400 Bad Request - If the provided data is invalid<br />

**Get account**
----
  Get account by its id

* **URL**

  /api/account/{id}

* **Method:**

  `GET`
  
* **Example Response Body**

  ```
    {"id":3,"balance":14.00,"number":"444"}
  ```
  

* **Success Response:**

  * **Code:** 200 Ok<br />
 
* **Error Response:**

  * **Code:** 404 Not Found <br />
  
**List accounts**
----
  List all accounts in the sytem

* **URL**

  /api/account

* **Method:**

  `GET`
  
* **Example Response Body**

  ```
    [
        {
            "id": 1,
            "balance": 1000.11,
            "number": "222"
        },
        {
            "id": 2,
            "balance": 9099987.35,
            "number": "333"
        },
        {
            "id": 3,
            "balance": 0,
            "number": "444"
        },
        {
            "id": 4,
            "balance": 150,
            "number": "555"
        },
        {
            "id": 5,
            "balance": 100.35,
            "number": "990"
        }
    ]
  ```
  

* **Success Response:**

  * **Code:** 200 Ok<br />
 
 
 **Delete account**
 ----
   Delete account by its id
 
 * **URL**
 
   /api/account/{id}
 
 * **Method:**
 
   `DELETE`
   
 
 * **Success Response:**
 
   * **Code:** 204 No Content<br />

**Transfer funds**
----
  Transfer funds between two accounts

* **URL**

  /api/transfer

* **Method:**

  `POST`
  

* **Example Request Body**

  ```
  {
    "amount": 100,
    "from": 333, 
    "to": 222
  }
     ```

* **Required request header**
     
  `Content-Type: application/json`

* **Success Response:**

  * **Code:** 200 Ok<br />
    **Body:** `{"success":true,"amount":"100","fromAcc":"333","toAcc":"222"}`
 
* **Error Response:**

  * **Code:** 400 Bad Request - If one of the accounts specified in request can not be found<br />
  * **Code:** 406 Not Acceptable - If source account doesn't have enough funds to perform transfer<br />
  
